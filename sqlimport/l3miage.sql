-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 22 mars 2019 à 17:34
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `l3miage`
--

-- --------------------------------------------------------

--
-- Structure de la table `affecte`
--

CREATE TABLE `affecte` (
  `idSection` int(11) NOT NULL,
  `idUE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `composante`
--

CREATE TABLE `composante` (
  `idComposante` int(11) NOT NULL,
  `LibelleComposante` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `composante`
--

INSERT INTO `composante` (`idComposante`, `LibelleComposante`) VALUES
(1, 'exterieur'),
(2, 'polytech'),
(3, 'PD info'),
(4, 'PD maths'),
(5, 'PD anglais'),
(6, 'PD staps'),
(7, 'INSA'),
(8, 'exterieur'),
(9, 'polytech'),
(10, 'PD info'),
(11, 'PD maths'),
(12, 'PD anglais'),
(13, 'PD staps'),
(14, 'INSA'),
(15, 'exterieur'),
(16, 'polytech'),
(17, 'PD info'),
(18, 'PD maths'),
(19, 'PD anglais'),
(20, 'PD staps'),
(21, 'INSA'),
(22, 'exterieur'),
(23, 'polytech'),
(24, 'PD info'),
(25, 'PD maths'),
(26, 'PD anglais'),
(27, 'PD staps'),
(28, 'INSA'),
(29, 'exterieur'),
(30, 'polytech'),
(31, 'PD info'),
(32, 'PD maths'),
(33, 'PD anglais'),
(34, 'PD staps'),
(35, 'INSA'),
(36, 'exterieur'),
(37, 'polytech'),
(38, 'PD info'),
(39, 'PD maths'),
(40, 'PD anglais'),
(41, 'PD staps'),
(42, 'INSA');

-- --------------------------------------------------------

--
-- Structure de la table `decharge`
--

CREATE TABLE `decharge` (
  `idDecharge` int(11) NOT NULL,
  `LibDecharge` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VolumeDecharge` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `decharge`
--

INSERT INTO `decharge` (`idDecharge`, `LibDecharge`, `VolumeDecharge`) VALUES
(1, 'decharge admin', 96),
(2, '2eme annnee jeune\r\nMCF', 32),
(3, 'Vp', 96),
(4, 'decharge admin', 96),
(5, '2eme annnee jeune\r\nMCF', 32),
(6, 'Vp', 96),
(7, 'decharge admin', 96),
(8, '2eme annnee jeune\r\nMCF', 32),
(9, 'Vp', 96),
(10, 'decharge admin', 96),
(11, '2eme annnee jeune\r\nMCF', 32),
(12, 'Vp', 96),
(13, 'decharge admin', 96),
(14, '2eme annnee jeune\r\nMCF', 32),
(15, 'Vp', 96),
(16, 'decharge admin', 96),
(17, '2eme annnee jeune\r\nMCF', 32),
(18, 'Vp', 96);

-- --------------------------------------------------------

--
-- Structure de la table `detient`
--

CREATE TABLE `detient` (
  `idUE` int(11) NOT NULL,
  `idType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `droit`
--

CREATE TABLE `droit` (
  `idDroit` int(11) NOT NULL,
  `LibelleDroit` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `droit`
--

INSERT INTO `droit` (`idDroit`, `LibelleDroit`) VALUES
(1, 'ADMINISTRATEUR'),
(2, 'RESPONSABLE_UFR'),
(3, 'ENSEIGNANTS'),
(4, 'RESPONSABLE_ANNEE'),
(5, 'ADMINISTRATEUR'),
(6, 'RESPONSABLE_UFR'),
(7, 'ENSEIGNANTS'),
(8, 'RESPONSABLE_ANNEE'),
(9, 'ADMINISTRATEUR'),
(10, 'RESPONSABLE_UFR'),
(11, 'ENSEIGNANTS'),
(12, 'RESPONSABLE_ANNEE'),
(13, 'ADMINISTRATEUR'),
(14, 'RESPONSABLE_UFR'),
(15, 'ENSEIGNANTS'),
(16, 'RESPONSABLE_ANNEE'),
(17, 'ADMINISTRATEUR'),
(18, 'RESPONSABLE_UFR'),
(19, 'ENSEIGNANTS'),
(20, 'RESPONSABLE_ANNEE'),
(21, 'ADMINISTRATEUR'),
(22, 'RESPONSABLE_UFR'),
(23, 'ENSEIGNANTS'),
(24, 'RESPONSABLE_ANNEE');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE `enseignant` (
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `idEnseignant` int(11) NOT NULL,
  `Nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VolAFaire` int(11) DEFAULT NULL,
  `VolAffecte` int(11) DEFAULT NULL,
  `HeureCompl` int(11) DEFAULT NULL,
  `idComposante` int(11) DEFAULT NULL,
  `idDecharge` int(11) DEFAULT NULL,
  `idDroit` int(11) DEFAULT NULL,
  `idStatut` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `enseignant`
--

INSERT INTO `enseignant` (`username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `idEnseignant`, `Nom`, `Prenom`, `VolAFaire`, `VolAffecte`, `HeureCompl`, `idComposante`, `idDecharge`, `idDroit`, `idStatut`) VALUES
('yo', 'yo', 'rouget.florent@outlook.fr', 'rouget.florent@outlook.fr', 1, NULL, '$2y$13$r1r1eOY42nu4O9ZlwDEwzu7Vnbeom9CHRxRk0HU6mqY4qEFVgBzYW', '2019-03-19 14:44:39', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 2, 'BOICHUT', 'Yohan', 21, 14, 0, NULL, NULL, NULL, NULL),
('ra', 'ra', 'ok@ok.ok', 'ok@ok.ok', 1, NULL, '$2y$13$wvBOppuMC2/22hwhrll1veCYLlQWQf12/3sfo.uXud/9mYuzD37hm', '2019-03-19 14:49:57', NULL, NULL, 'a:2:{i:0;s:14:\"ROLE_ENSEIGANT\";i:1;s:15:\"ROLE_ENSEIGNANT\";}', 3, 'RAKOTOZAFY', 'Raymond', NULL, NULL, 4, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `enseigne`
--

CREATE TABLE `enseigne` (
  `enseignant_id` int(11) NOT NULL,
  `uniteenseigenement_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `enseigne`
--

INSERT INTO `enseigne` (`enseignant_id`, `uniteenseigenement_id`) VALUES
(2, 25),
(2, 27),
(2, 28),
(3, 25);

-- --------------------------------------------------------

--
-- Structure de la table `est_responsable_de`
--

CREATE TABLE `est_responsable_de` (
  `enseignant_id` int(11) NOT NULL,
  `uniteenseigenementresponsable_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `est_responsable_de`
--

INSERT INTO `est_responsable_de` (`enseignant_id`, `uniteenseigenementresponsable_id`) VALUES
(2, 27),
(2, 28),
(3, 25);

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `idFormation` int(11) NOT NULL,
  `nomFormation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NumSemestre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`idFormation`, `nomFormation`, `NumSemestre`) VALUES
(1, 'L3 informatique', 5),
(2, 'L3 informatique', 6),
(3, 'L3 informatique', 5),
(4, 'L3 informatique', 6),
(5, 'L3 informatique', 5),
(6, 'L3 informatique', 6),
(7, 'L3 informatique', 5),
(8, 'L3 informatique', 6),
(9, 'L3 informatique', 5),
(10, 'L3 informatique', 6);

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

CREATE TABLE `section` (
  `idSection` int(11) NOT NULL,
  `NomSection` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CodeEtape` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idFormation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `section`
--

INSERT INTO `section` (`idSection`, `NomSection`, `CodeEtape`, `idFormation`) VALUES
(1, 'MIAGE', 'SL3GI8', 1),
(2, 'MIAGE', 'SL3GI8', 1),
(3, 'MIAGE', 'SL3GI8', 1),
(4, 'MIAGE', 'SL3GI8', 1),
(5, 'MIAGE', 'SL3GI8', 1);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `idStatut` int(11) NOT NULL,
  `LibelleStatut` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VolumeStatuaire` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`idStatut`, `LibelleStatut`, `VolumeStatuaire`) VALUES
(1, 'MCF', 192),
(2, 'PU', 192),
(3, 'ATER', 192),
(4, 'DMCE', 64),
(5, 'vacataire', 0),
(6, 'MCF', 192),
(7, 'PU', 192),
(8, 'ATER', 192),
(9, 'DMCE', 64),
(10, 'vacataire', 0),
(11, 'MCF', 192),
(12, 'PU', 192),
(13, 'ATER', 192),
(14, 'DMCE', 64),
(15, 'vacataire', 0),
(16, 'MCF', 192),
(17, 'PU', 192),
(18, 'ATER', 192),
(19, 'DMCE', 64),
(20, 'vacataire', 0),
(21, 'MCF', 192),
(22, 'PU', 192),
(23, 'ATER', 192),
(24, 'DMCE', 64),
(25, 'vacataire', 0),
(26, 'MCF', 192),
(27, 'PU', 192),
(28, 'ATER', 192),
(29, 'DMCE', 64),
(30, 'vacataire', 0);

-- --------------------------------------------------------

--
-- Structure de la table `typecours`
--

CREATE TABLE `typecours` (
  `idType` int(11) NOT NULL,
  `nomType` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valeurcoef` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `typecours`
--

INSERT INTO `typecours` (`idType`, `nomType`, `valeurcoef`) VALUES
(1, 'CM', 1),
(2, 'TP', 1),
(3, 'TD', 1),
(4, 'CM', 1),
(5, 'TP', 1),
(6, 'TD', 1),
(7, 'CM', 1),
(8, 'TP', 1),
(9, 'TD', 1),
(10, 'CM', 1),
(11, 'TP', 1),
(12, 'TD', 1),
(13, 'CM', 1),
(14, 'TP', 1),
(15, 'TD', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ufr`
--

CREATE TABLE `ufr` (
  `codeUFR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `ufr`
--

INSERT INTO `ufr` (`codeUFR`) VALUES
(903),
(905);

-- --------------------------------------------------------

--
-- Structure de la table `uniteenseignement`
--

CREATE TABLE `uniteenseignement` (
  `idUE` int(11) NOT NULL,
  `nomUE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `codeELP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valider` tinyint(1) NOT NULL DEFAULT '0',
  `codeUFR` int(11) DEFAULT NULL,
  `Volaffecte` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `uniteenseignement`
--

INSERT INTO `uniteenseignement` (`idUE`, `nomUE`, `codeELP`, `valider`, `codeUFR`, `Volaffecte`) VALUES
(25, 'Programmation structurée', 'SLA1INFO', 0, NULL, 4),
(27, 'Programmation avancée', 'SLA5IF04', 1, NULL, 6),
(28, 'ok', 'ok', 0, NULL, 8);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affecte`
--
ALTER TABLE `affecte`
  ADD PRIMARY KEY (`idSection`,`idUE`),
  ADD KEY `IDX_4890ADE83DCA111D` (`idSection`),
  ADD KEY `IDX_4890ADE8E9CE1867` (`idUE`);

--
-- Index pour la table `composante`
--
ALTER TABLE `composante`
  ADD PRIMARY KEY (`idComposante`);

--
-- Index pour la table `decharge`
--
ALTER TABLE `decharge`
  ADD PRIMARY KEY (`idDecharge`);

--
-- Index pour la table `detient`
--
ALTER TABLE `detient`
  ADD PRIMARY KEY (`idUE`,`idType`),
  ADD KEY `IDX_12DA2D20E9CE1867` (`idUE`),
  ADD KEY `IDX_12DA2D20FF2309B7` (`idType`);

--
-- Index pour la table `droit`
--
ALTER TABLE `droit`
  ADD PRIMARY KEY (`idDroit`);

--
-- Index pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`idEnseignant`),
  ADD UNIQUE KEY `UNIQ_81A72FA192FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_81A72FA1A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_81A72FA1C05FB297` (`confirmation_token`),
  ADD KEY `IDX_81A72FA18F7A9A22` (`idComposante`),
  ADD KEY `IDX_81A72FA15F073DA6` (`idDecharge`),
  ADD KEY `IDX_81A72FA1DCBEE44C` (`idDroit`),
  ADD KEY `IDX_81A72FA186755825` (`idStatut`);

--
-- Index pour la table `enseigne`
--
ALTER TABLE `enseigne`
  ADD PRIMARY KEY (`enseignant_id`,`uniteenseigenement_id`),
  ADD KEY `IDX_37D4778EE455FCC0` (`enseignant_id`),
  ADD KEY `IDX_37D4778E55A2311E` (`uniteenseigenement_id`);

--
-- Index pour la table `est_responsable_de`
--
ALTER TABLE `est_responsable_de`
  ADD PRIMARY KEY (`enseignant_id`,`uniteenseigenementresponsable_id`),
  ADD KEY `IDX_C2958152E455FCC0` (`enseignant_id`),
  ADD KEY `IDX_C29581529671FCF0` (`uniteenseigenementresponsable_id`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`idFormation`);

--
-- Index pour la table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`idSection`),
  ADD KEY `Section_Formation_FK` (`idFormation`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`idStatut`);

--
-- Index pour la table `typecours`
--
ALTER TABLE `typecours`
  ADD PRIMARY KEY (`idType`);

--
-- Index pour la table `ufr`
--
ALTER TABLE `ufr`
  ADD PRIMARY KEY (`codeUFR`);

--
-- Index pour la table `uniteenseignement`
--
ALTER TABLE `uniteenseignement`
  ADD PRIMARY KEY (`idUE`),
  ADD KEY `IDX_E4637952F7EBD635` (`codeUFR`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `composante`
--
ALTER TABLE `composante`
  MODIFY `idComposante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `decharge`
--
ALTER TABLE `decharge`
  MODIFY `idDecharge` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `droit`
--
ALTER TABLE `droit`
  MODIFY `idDroit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `enseignant`
--
ALTER TABLE `enseignant`
  MODIFY `idEnseignant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `formation`
--
ALTER TABLE `formation`
  MODIFY `idFormation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `section`
--
ALTER TABLE `section`
  MODIFY `idSection` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `statut`
--
ALTER TABLE `statut`
  MODIFY `idStatut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `typecours`
--
ALTER TABLE `typecours`
  MODIFY `idType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `ufr`
--
ALTER TABLE `ufr`
  MODIFY `codeUFR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=906;

--
-- AUTO_INCREMENT pour la table `uniteenseignement`
--
ALTER TABLE `uniteenseignement`
  MODIFY `idUE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `affecte`
--
ALTER TABLE `affecte`
  ADD CONSTRAINT `FK_4890ADE83DCA111D` FOREIGN KEY (`idSection`) REFERENCES `section` (`idSection`),
  ADD CONSTRAINT `FK_4890ADE8E9CE1867` FOREIGN KEY (`idUE`) REFERENCES `uniteenseignement` (`idUE`);

--
-- Contraintes pour la table `detient`
--
ALTER TABLE `detient`
  ADD CONSTRAINT `FK_12DA2D20E9CE1867` FOREIGN KEY (`idUE`) REFERENCES `uniteenseignement` (`idUE`),
  ADD CONSTRAINT `FK_12DA2D20FF2309B7` FOREIGN KEY (`idType`) REFERENCES `typecours` (`idType`);

--
-- Contraintes pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD CONSTRAINT `FK_81A72FA15F073DA6` FOREIGN KEY (`idDecharge`) REFERENCES `decharge` (`idDecharge`),
  ADD CONSTRAINT `FK_81A72FA186755825` FOREIGN KEY (`idStatut`) REFERENCES `statut` (`idStatut`),
  ADD CONSTRAINT `FK_81A72FA18F7A9A22` FOREIGN KEY (`idComposante`) REFERENCES `composante` (`idComposante`),
  ADD CONSTRAINT `FK_81A72FA1DCBEE44C` FOREIGN KEY (`idDroit`) REFERENCES `droit` (`idDroit`);

--
-- Contraintes pour la table `enseigne`
--
ALTER TABLE `enseigne`
  ADD CONSTRAINT `FK_37D4778E55A2311E` FOREIGN KEY (`uniteenseigenement_id`) REFERENCES `uniteenseignement` (`idUE`),
  ADD CONSTRAINT `FK_37D4778EE455FCC0` FOREIGN KEY (`enseignant_id`) REFERENCES `enseignant` (`idEnseignant`);

--
-- Contraintes pour la table `est_responsable_de`
--
ALTER TABLE `est_responsable_de`
  ADD CONSTRAINT `FK_C29581529671FCF0` FOREIGN KEY (`uniteenseigenementresponsable_id`) REFERENCES `uniteenseignement` (`idUE`),
  ADD CONSTRAINT `FK_C2958152E455FCC0` FOREIGN KEY (`enseignant_id`) REFERENCES `enseignant` (`idEnseignant`);

--
-- Contraintes pour la table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `FK_2D737AEFBCAA0AE9` FOREIGN KEY (`idFormation`) REFERENCES `formation` (`idFormation`);

--
-- Contraintes pour la table `uniteenseignement`
--
ALTER TABLE `uniteenseignement`
  ADD CONSTRAINT `FK_E4637952F7EBD635` FOREIGN KEY (`codeUFR`) REFERENCES `ufr` (`codeUFR`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
