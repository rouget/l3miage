<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use http\Exception;
use FOS\UserBundle\Model\User as BaseUser;
//, indexes={@ORM\Index(name="Enseignant_Composante_FK", columns={"idComposante"}), @ORM\Index(name="Enseignant_Statut2_FK", columns={"idStatut"}), @ORM\Index(name="Enseignant_Decharge1_FK", columns={"idDecharge"}), @ORM\Index(name="Enseignant_Droit0_FK", columns={"idDroit"})}
/**
 * Enseignant
 *
 * @ORM\Table(name="enseignant")
 * @ORM\Entity
 */
class Enseignant extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="idEnseignant", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=50, nullable=false)
     */
    private $prenom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="VolAFaire", type="integer", nullable=true, options={"default"=NULL})
     */
    private $volafaire = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="VolAffecte", type="integer", nullable=true, options={"default"=NULL})
     */
    private $volaffecte = NULL;

    /**
     * @var int|null
     *
     * @ORM\Column(name="HeureCompl", type="integer", nullable=true, options={"default"=NULL})
     */
    private $heurecompl = NULL;

    /**
     * @var \Composante
     *
     * @ORM\ManyToOne(targetEntity="Composante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idComposante", referencedColumnName="idComposante")
     * })
     */
    private $idcomposante;

    /**
     * @var \Decharge
     *
     * @ORM\ManyToOne(targetEntity="Decharge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDecharge", referencedColumnName="idDecharge")
     * })
     */
    private $iddecharge;

    /**
     * @var \Droit
     *
     * @ORM\ManyToOne(targetEntity="Droit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDroit", referencedColumnName="idDroit")
     * })
     */
    private $iddroit;

    /**
     * @var \Statut
     *
     * @ORM\ManyToOne(targetEntity="Statut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idStatut", referencedColumnName="idStatut")
     * })
     */
    private $idstatut;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Uniteenseignement", inversedBy="enseignant",cascade = {"persist","merge"})
     * @ORM\JoinTable(name="enseigne",
     *     joinColumns = {@ORM\JoinColumn(name="enseignant_id", referencedColumnName="idEnseignant")},
     *     inverseJoinColumns = {@ORM\JoinColumn(name="uniteenseigenement_id", referencedColumnName="idUE")})
     */
    private $uniteenseigenement;
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Uniteenseignement", inversedBy="est_responsable_de",cascade = {"persist","merge"})
     * @ORM\JoinTable(name="est_responsable_de",
     *     joinColumns = {@ORM\JoinColumn(name="enseignant_id", referencedColumnName="idEnseignant")},
     *     inverseJoinColumns = {@ORM\JoinColumn(name="uniteenseigenementresponsable_id", referencedColumnName="idUE")})
     */
    private $uniteenseigenement_responsable;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->uniteenseigenement = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uniteenseigenement_responsable = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nom = " ";
        $this->prenom = " ";
        $this->heurecompl = 0;


    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return int|null
     */
    public function getVolafaire(): ?int
    {
        return $this->volafaire;
    }

    /**
     * @param int|null $volafaire
     */
    public function setVolafaire(?int $volafaire): void
    {
        $this->volafaire = $volafaire;
    }

    /**
     * @return int|null
     */
    public function getVolaffecte(): ?int
    {
        return $this->volaffecte;
    }

    /**
     * @param int|null $volaffecte
     */
    public function setVolaffecte(?int $volaffecte): void
    {
        $this->volaffecte = $volaffecte;
    }

    /**
     * @return int|null
     */
    public function getHeurecompl(): ?int
    {
        return $this->heurecompl;
    }

    /**
     * @param int|null $heurecompl
     */
    public function setHeurecompl(?int $heurecompl): void
    {
        $this->heurecompl = $heurecompl;
    }

    /**
     * @return \Composante
     */
    public function getIdcomposante(): \Composante
    {
        return $this->idcomposante;
    }

    /**
     * @param \Composante $idcomposante
     */
    public function setIdcomposante(\Composante $idcomposante): void
    {
        $this->idcomposante = $idcomposante;
    }

    /**
     * @return \Decharge
     */
    public function getIddecharge(): \Decharge
    {
        return $this->iddecharge;
    }

    /**
     * @param \Decharge $iddecharge
     */
    public function setIddecharge(\Decharge $iddecharge): void
    {
        $this->iddecharge = $iddecharge;
    }

    /**
     * @return \Droit
     */
    public function getIddroit(): \Droit
    {
        return $this->iddroit;
    }

    /**
     * @param \Droit $iddroit
     */
    public function setIddroit(\Droit $iddroit): void
    {
        $this->iddroit = $iddroit;
    }

    /**
     * @return \Statut
     */
    public function getIdstatut(): \Statut
    {
        return $this->idstatut;
    }

    /**
     * @param \Statut $idstatut
     */
    public function setIdstatut(\Statut $idstatut): void
    {
        $this->idstatut = $idstatut;
    }



    /**
     * @return Collection|Uniteenseignement[]
     */
    public function getUniteenseigenement() :Collection
    {
        return $this->uniteenseigenement;
    }

    public function addUniteenseigenement(Uniteenseignement $uniteenseignement): self
    {
        if($this->uniteenseigenement->contains($uniteenseignement)){
            return $this;
        }
        $this->uniteenseigenement->add($uniteenseignement);
        $uniteenseignement->addEnseignant($this);
        return $this;
    }
    public function removeUniteenseigenement(Uniteenseignement $uniteenseignement): self
    {
        if(!$this->uniteenseigenement->contains($uniteenseignement)){
            return $this;
        }
        $this->uniteenseigenement->removeElement($uniteenseignement);
        $uniteenseignement->removeEnseignant($this);
    }

    /**
     * @return Collection|Uniteenseignement[]
     */
    public function getUniteenseigenementResponsable() :Collection
    {
        return $this->uniteenseigenement_responsable;
    }

    public function addUniteenseigenementResponsable(Uniteenseignement $uniteenseigenement_responsable): self
    {
        if($this->uniteenseigenement_responsable->contains($uniteenseigenement_responsable)){
            return $this;
        }
        $this->uniteenseigenement_responsable->add($uniteenseigenement_responsable);
        $uniteenseigenement_responsable->addEnseignant($this);
        return $this;
    }
    public function removeUniteenseigenementResponsable(Uniteenseignement $uniteenseigenement_responsable): self
    {
        if(!$this->uniteenseigenement_responsable->contains($uniteenseigenement_responsable)){
            return $this;
        }
        $this->uniteenseigenement_responsable->removeElement($uniteenseigenement_responsable);
        $uniteenseigenement_responsable->removeEnseignant($this);
    }




    public function __toString()
    {

        try {
            if($this->nom) {
                return $this->nom;
            }}
        catch (Exception $exception){
            return '';
        }

    }

}
