<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Droit
 *
 * @ORM\Table(name="droit")
 * @ORM\Entity
 */
class Droit
{
    /**
     * @var int
     *
     * @ORM\Column(name="idDroit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddroit;

    /**
     * @var string
     *
     * @ORM\Column(name="LibelleDroit", type="string", length=50, nullable=false)
     */
    private $libelledroit;

    /**
     * @return int
     */
    public function getIddroit(): int
    {
        return $this->iddroit;
    }

    /**
     * @param int $iddroit
     */
    public function setIddroit(int $iddroit): void
    {
        $this->iddroit = $iddroit;
    }

    /**
     * @return string
     */
    public function getLibelledroit(): string
    {
        return $this->libelledroit;
    }

    /**
     * @param string $libelledroit
     */
    public function setLibelledroit(string $libelledroit): void
    {
        $this->libelledroit = $libelledroit;
    }


}
