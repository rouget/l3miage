<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formation
 *
 * @ORM\Table(name="formation")
 * @ORM\Entity
 */
class Formation
{
    /**
     * @var int
     *
     * @ORM\Column(name="idFormation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idformation;

    /**
     * @var string
     *
     * @ORM\Column(name="nomFormation", type="string", length=50, nullable=false)
     */
    private $nomformation;

    /**
     * @var int
     *
     * @ORM\Column(name="NumSemestre", type="integer", nullable=false)
     */
    private $numsemestre;

    /**
     * @return int
     */
    public function getIdformation(): int
    {
        return $this->idformation;
    }

    /**
     * @param int $idformation
     */
    public function setIdformation(int $idformation): void
    {
        $this->idformation = $idformation;
    }

    /**
     * @return string
     */
    public function getNomformation(): string
    {
        return $this->nomformation;
    }

    /**
     * @param string $nomformation
     */
    public function setNomformation(string $nomformation): void
    {
        $this->nomformation = $nomformation;
    }

    /**
     * @return int
     */
    public function getNumsemestre(): int
    {
        return $this->numsemestre;
    }

    /**
     * @param int $numsemestre
     */
    public function setNumsemestre(int $numsemestre): void
    {
        $this->numsemestre = $numsemestre;
    }


}
