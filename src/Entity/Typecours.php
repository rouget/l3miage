<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Typecours
 *
 * @ORM\Table(name="typecours")
 * @ORM\Entity
 */
class Typecours
{
    /**
     * @var int
     *
     * @ORM\Column(name="idType", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtype;

    /**
     * @var string
     *
     * @ORM\Column(name="nomType", type="string", length=50, nullable=false)
     */
    private $nomtype;

    /**
     * @var int
     *
     * @ORM\Column(name="valeurcoef", type="integer", nullable=false)
     */
    private $valeurcoef;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Uniteenseignement", mappedBy="idtype")
     */
    private $idue;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdtype(): int
    {
        return $this->idtype;
    }

    /**
     * @param int $idtype
     */
    public function setIdtype(int $idtype): void
    {
        $this->idtype = $idtype;
    }

    /**
     * @return string
     */
    public function getNomtype(): string
    {
        return $this->nomtype;
    }

    /**
     * @param string $nomtype
     */
    public function setNomtype(string $nomtype): void
    {
        $this->nomtype = $nomtype;
    }

    /**
     * @return int
     */
    public function getValeurcoef(): int
    {
        return $this->valeurcoef;
    }

    /**
     * @param int $valeurcoef
     */
    public function setValeurcoef(int $valeurcoef): void
    {
        $this->valeurcoef = $valeurcoef;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdue(): \Doctrine\Common\Collections\Collection
    {
        return $this->idue;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idue
     */
    public function setIdue(\Doctrine\Common\Collections\Collection $idue): void
    {
        $this->idue = $idue;
    }

    public function addIdue(Uniteenseignement $idue): self
    {
        if (!$this->idue->contains($idue)) {
            $this->idue[] = $idue;
            $idue->addIdtype($this);
        }

        return $this;
    }

    public function removeIdue(Uniteenseignement $idue): self
    {
        if ($this->idue->contains($idue)) {
            $this->idue->removeElement($idue);
            $idue->removeIdtype($this);
        }

        return $this;
    }

}
