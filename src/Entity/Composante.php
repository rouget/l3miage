<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Composante
 *
 * @ORM\Table(name="composante")
 * @ORM\Entity
 */
class Composante
{
    /**
     * @var int
     *
     * @ORM\Column(name="idComposante", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcomposante;

    /**
     * @var string
     *
     * @ORM\Column(name="LibelleComposante", type="string", length=50, nullable=false)
     */
    private $libellecomposante;

    /**
     * @return int
     */
    public function getIdcomposante(): int
    {
        return $this->idcomposante;
    }

    /**
     * @param int $idcomposante
     */
    public function setIdcomposante(int $idcomposante): void
    {
        $this->idcomposante = $idcomposante;
    }

    /**
     * @return string
     */
    public function getLibellecomposante(): string
    {
        return $this->libellecomposante;
    }

    /**
     * @param string $libellecomposante
     */
    public function setLibellecomposante(string $libellecomposante): void
    {
        $this->libellecomposante = $libellecomposante;
    }


}
