<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use http\Exception;

/**
 * Ufr
 *
 * @ORM\Table(name="ufr")
 * @ORM\Entity
 */
class Ufr
{
    /**
     * @var int
     *
     * @ORM\Column(name="codeUFR", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codeufr;

    /**
     * @return int
     */
    public function getCodeufr(): int
    {
        return $this->codeufr;
    }

    /**
     * @param int $codeufr
     */
    public function setCodeufr(int $codeufr): void
    {
        $this->codeufr = $codeufr;
    }

    public function __toString()
    {
        try {
        if($this->codeufr) {
            return (string)$this->codeufr;
        }}
        catch (Exception $exception){
            return '';
        }

    }


}
