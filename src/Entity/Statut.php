<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statut
 *
 * @ORM\Table(name="statut")
 * @ORM\Entity
 */
class Statut
{
    /**
     * @var int
     *
     * @ORM\Column(name="idStatut", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idstatut;

    /**
     * @var string
     *
     * @ORM\Column(name="LibelleStatut", type="string", length=50, nullable=false)
     */
    private $libellestatut;

    /**
     * @var int|null
     *
     * @ORM\Column(name="VolumeStatuaire", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $volumestatuaire = 'NULL';

    /**
     * @return int
     */
    public function getIdstatut(): int
    {
        return $this->idstatut;
    }

    /**
     * @param int $idstatut
     */
    public function setIdstatut(int $idstatut): void
    {
        $this->idstatut = $idstatut;
    }

    /**
     * @return string
     */
    public function getLibellestatut(): string
    {
        return $this->libellestatut;
    }

    /**
     * @param string $libellestatut
     */
    public function setLibellestatut(string $libellestatut): void
    {
        $this->libellestatut = $libellestatut;
    }

    /**
     * @return int|null
     */
    public function getVolumestatuaire(): ?int
    {
        return $this->volumestatuaire;
    }

    /**
     * @param int|null $volumestatuaire
     */
    public function setVolumestatuaire(?int $volumestatuaire): void
    {
        $this->volumestatuaire = $volumestatuaire;
    }


}
