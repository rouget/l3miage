<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use http\Exception;

//, indexes={@ORM\Index(name="UniteEnseignement_UFR_FK", columns={"codeUFR"})}

/**
 * Uniteenseignement
 *
 *
 * @property int Volaffecte
 * @ORM\Table(name="uniteenseignement")
 * @ORM\Entity
 */
class Uniteenseignement
{
    /**
     * @var int
     *
     * @ORM\Column(name="idUE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idue;

    /**
     * @var string
     *
     * @ORM\Column(name="nomUE", type="string", length=50, nullable=false)
     */
    private $nomue;

    /**
     * @var string
     *
     * @ORM\Column(name="codeELP", type="string", length=50, nullable=false)
     */
    private $codeelp;

    /**
     * @var \Ufr
     *
     * @ORM\ManyToOne(targetEntity="Ufr")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codeUFR", referencedColumnName="codeUFR")
     * })
     */
    private $codeufr;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Section", mappedBy="idue")
     */
    private $idsection;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Typecours", inversedBy="idue")
     * @ORM\JoinTable(name="detient",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idUE", referencedColumnName="idUE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idType", referencedColumnName="idType")
     *   }
     * )
     */
    private $idtype;

    /**
     * @var \Doctrine\Common\Collections\Collection|Enseignant[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", mappedBy="uniteenseigenement",cascade = {"persist","merge"} )
     */
    private $enseignant;

    /**
     * @var \Doctrine\Common\Collections\Collection|Enseignant[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", mappedBy="uniteenseigenement_responsable",cascade = {"persist","merge"} )
     */
    private $est_responsable_de;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valider", type="boolean", length=50,options={"default":false})
     */
    private $valider = false ;
    /**
     * @var integer
     *
     * @ORM\Column(name="Volaffecte", type="integer", length=50,options={"default":0})
     */
    private $Volaffecte = 0;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idsection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idtype = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enseignant = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nomue = " ";
        $this->codeelp = "";
        $this->Volaffecte = 0;
        //$this->valider = false;
        $this->est_responsable_de = new \Doctrine\Common\Collections\ArrayCollection();


    }

    /**
     * @return int
     */
    public function getVolaffecte(): int
    {
        return $this->Volaffecte;
    }

    /**
     * @param int $Volaffecte
     */
    public function setVolaffecte(int $Volaffecte): void
    {
        $this->Volaffecte = $Volaffecte;
    }





    /**
     * @return int
     */
    public function getIdue(): int
    {
        return $this->idue;
    }

    /**
     * @param int $idue
     */
    public function setIdue(int $idue): void
    {
        $this->idue = $idue;
    }

    /**
     * @return string
     */
    public function getNomue(): string
    {
        return $this->nomue;
    }

    /**
     * @param string $nomue
     */
    public function setNomue(string $nomue): void
    {
        $this->nomue = $nomue;
    }

    /**
     * @return string
     */
    public function getCodeelp(): string
    {
        return $this->codeelp;
    }

    /**
     * @param string $codeelp
     */
    public function setCodeelp(string $codeelp): void
    {
        $this->codeelp = $codeelp;
    }

    /**
     * @return \Ufr
     */
    public function getCodeufr(): \Ufr
    {
        return $this->codeufr;
    }

    /**
     * @param \Ufr $codeufr
     */
    public function setCodeufr(\Ufr $codeufr): void
    {
        $this->codeufr = $codeufr;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdsection(): \Doctrine\Common\Collections\Collection
    {
        return $this->idsection;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idsection
     */
    public function setIdsection(\Doctrine\Common\Collections\Collection $idsection): void
    {
        $this->idsection = $idsection;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdtype(): \Doctrine\Common\Collections\Collection
    {
        return $this->idtype;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idtype
     */
    public function setIdtype(\Doctrine\Common\Collections\Collection $idtype): void
    {
        $this->idtype = $idtype;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignant() :Collection
    {
        return $this->enseignant;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if($this->enseignant->contains($enseignant)){
            return $this;
        }
        $this->enseignant->add($enseignant);
        $enseignant->addUniteenseigenement($this);
        return $this;
    }
    public function removeEnseignant(Enseignant $enseignant): self
    {
        if(!$this->enseignant->contains($enseignant)){
            return $this;
        }
        $this->enseignant->removeElement($enseignant);
        $enseignant->removeUniteenseigenement($this);
    }

    /**
     * @return Enseignant[]|Collection
     */
    public function getEstResponsableDe():Collection
    {
        return $this->est_responsable_de;
    }
    public function addEstResponsableDe(Enseignant $est_responsable_de): self
    {
        if($this->est_responsable_de->contains($est_responsable_de)){
            return $this;
        }
        $this->est_responsable_de->add($est_responsable_de);
        $est_responsable_de->addUniteenseigenementResponsable($this);
        return $this;
    }
    public function removeEstResponsableDe(Enseignant $est_responsable_de): self
    {
        if(!$this->est_responsable_de->contains($est_responsable_de)){
            return $this;
        }
        $this->est_responsable_de->removeElement($est_responsable_de);
        $est_responsable_de->removeUniteenseigenementResponsable($this);
    }


    /**
     * @return bool
     */
    public function isValider(): bool
    {
        return $this->valider;
    }

    /**
     * @param bool $valider
     */
    public function setValider(bool $valider): void
    {
        $this->valider = $valider;
    }
    public function __toString()
    {

        try {
            if($this->nomue) {
                return $this->nomue;
            }}
        catch (Exception $exception){
            return '';
        }

    }
}
