<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Section
 *
 * @ORM\Table(name="section", indexes={@ORM\Index(name="Section_Formation_FK", columns={"idFormation"})})
 * @ORM\Entity
 */
class Section
{
    /**
     * @var int
     *
     * @ORM\Column(name="idSection", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsection;

    /**
     * @var string
     *
     * @ORM\Column(name="NomSection", type="string", length=50, nullable=false)
     */
    private $nomsection;

    /**
     * @var string
     *
     * @ORM\Column(name="CodeEtape", type="string", length=50, nullable=false)
     */
    private $codeetape;

    /**
     * @var \Formation
     *
     * @ORM\ManyToOne(targetEntity="Formation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFormation", referencedColumnName="idFormation")
     * })
     */
    private $idformation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Uniteenseignement", inversedBy="idsection")
     * @ORM\JoinTable(name="affecte",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idSection", referencedColumnName="idSection")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idUE", referencedColumnName="idUE")
     *   }
     * )
     */
    private $idue;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdsection(): int
    {
        return $this->idsection;
    }

    /**
     * @param int $idsection
     */
    public function setIdsection(int $idsection): void
    {
        $this->idsection = $idsection;
    }

    /**
     * @return string
     */
    public function getNomsection(): string
    {
        return $this->nomsection;
    }

    /**
     * @param string $nomsection
     */
    public function setNomsection(string $nomsection): void
    {
        $this->nomsection = $nomsection;
    }

    /**
     * @return string
     */
    public function getCodeetape(): string
    {
        return $this->codeetape;
    }

    /**
     * @param string $codeetape
     */
    public function setCodeetape(string $codeetape): void
    {
        $this->codeetape = $codeetape;
    }

    /**
     * @return \Formation
     */
    public function getIdformation(): \Formation
    {
        return $this->idformation;
    }

    /**
     * @param \Formation $idformation
     */
    public function setIdformation(\Formation $idformation): void
    {
        $this->idformation = $idformation;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdue(): \Doctrine\Common\Collections\Collection
    {
        return $this->idue;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $idue
     */
    public function setIdue(\Doctrine\Common\Collections\Collection $idue): void
    {
        $this->idue = $idue;
    }

    public function addIdue(Uniteenseignement $idue): self
    {
        if (!$this->idue->contains($idue)) {
            $this->idue[] = $idue;
        }

        return $this;
    }

    public function removeIdue(Uniteenseignement $idue): self
    {
        if ($this->idue->contains($idue)) {
            $this->idue->removeElement($idue);
        }

        return $this;
    }

}
