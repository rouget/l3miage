<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Decharge
 *
 * @ORM\Table(name="decharge")
 * @ORM\Entity
 */
class Decharge
{
    /**
     * @var int
     *
     * @ORM\Column(name="idDecharge", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddecharge;

    /**
     * @var string
     *
     * @ORM\Column(name="LibDecharge", type="string", length=50, nullable=false)
     */
    private $libdecharge;

    /**
     * @var int|null
     *
     * @ORM\Column(name="VolumeDecharge", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $volumedecharge = 'NULL';

    /**
     * @return int
     */
    public function getIddecharge(): int
    {
        return $this->iddecharge;
    }

    /**
     * @param int $iddecharge
     */
    public function setIddecharge(int $iddecharge): void
    {
        $this->iddecharge = $iddecharge;
    }

    /**
     * @return string
     */
    public function getLibdecharge(): string
    {
        return $this->libdecharge;
    }

    /**
     * @param string $libdecharge
     */
    public function setLibdecharge(string $libdecharge): void
    {
        $this->libdecharge = $libdecharge;
    }

    /**
     * @return int|null
     */
    public function getVolumedecharge(): ?int
    {
        return $this->volumedecharge;
    }

    /**
     * @param int|null $volumedecharge
     */
    public function setVolumedecharge(?int $volumedecharge): void
    {
        $this->volumedecharge = $volumedecharge;
    }


}
