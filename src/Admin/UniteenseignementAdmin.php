<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Enseignant;
use App\Entity\Uniteenseignement;
use phpDocumentor\Reflection\Types\Integer;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

final class UniteenseignementAdmin extends AbstractAdmin
{


    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('idue')
            ->add('nomue')
            ->add('codeelp')
            ->add('valider');
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $authorization_checker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $listMapper
            ->add('nomue')
            ->add('codeelp')
            ->add('enseignant')
            ->add('est_responsable_de')
            ->add('Volaffecte',null,[
                "label"=>'Nombre dheure'
            ])
            ->add('valider')

        ;

        if($authorization_checker->isGranted('ROLE_SUPER_ADMIN')){
        $listMapper
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
        }
        else{
            $listMapper
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                    ],
                ]);
        }
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $authorization_checker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');


        $formMapper
            ->tab('Ajouter Unité Enseignement')
                ->with('', array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid',
            ))
                ->add('nomue')
                ->add('codeelp')
                ->add('enseignant', EntityType::class ,[

                    'multiple' => true,
                    'class' => 'App\Entity\Enseignant',
                    'required' => false,
                    'placeholder' => 'Enseignant',
                    'label' => 'Enseignant',
                ])
                 ->add('est_responsable_de', EntityType::class ,[

                'multiple' => true,
                'class' => 'App\Entity\Enseignant',
                'required' => false,
                'placeholder' => 'Responsable',
                'label' => 'Responsable',
            ])
            ->add('Volaffecte',IntegerType::class,[
                'label' => 'Nombre dheure',

            ])
                ->end()
           /* ->add('codeufr', EntityType::class ,[
                'class' => 'App\Entity\Ufr',
                'required' => false,
                'placeholder' => 'Code UFR',
                'label' => false,
            ])*/
           ->end();


   if($authorization_checker->isGranted('ROLE_SUPER_ADMIN')){

       $formMapper
           ->tab('VALIDATION')
            ->with('', array(
                'class'       => 'col-md-6',
                'box_class'   => 'box box-solid',
            ))
                 ->add('valider')
            ->end()
            ->end()
            ;}
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('idue')
            ->add('nomue')
            ->add('codeelp')
            ->add('enseignant')
            ->add('est_responsable_de')
            ->add('Volaffecte')
            ;
    }

   public function prePersist($object)
    {

        $this->handleHeure($object);
        $this->handleEnseigne($object);
        $this->handleEstResponsableDe($object);

    }

    public function preValidate($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository($this->getClass());

    }

    public function preUpdate($object)
    {

        $this->handleEnseigne($object);
        $this->handleEstResponsableDe($object);





    }
    public function preDelete($object){
        $this->delEnsignant($object);
        $this->delResponsable($object);
    }

    protected function handleEnseigne($object)
    {
       foreach ($object->getEnseignant() as $reponse ){
           $reponse->addUniteenseigenement($object);
       }
    }
    protected function handleHeure($object)
    {
        foreach ($object->getEnseignant() as $reponse ){
            if($reponse->getVolafaire() >= $object->getVolaffecte() || $reponse->getVolafaire()==0){
                if($reponse->getVolafaire()== 0 ){
                    $reponse->setHeurecompl($reponse->getHeurecompl()+$object->getVolaffecte());
                }else{
                    $reponse->setVolafaire($reponse->getVolafaire()-$object->getVolaffecte());
                    $reponse->setVolaffecte($reponse->getVolaffecte()+$object->getVolaffecte());
                }


            }




        }
    }
    protected function handleEstResponsableDe($object)
    {
        foreach ($object->getEstResponsableDe() as $reponse ){
            $reponse->addUniteenseigenementResponsable($object);
        }
    }

    protected function delEnsignant($object)
    {
        foreach ($object->getEnseignant() as $reponse ){
            $reponse->removeUniteenseigenement($object);
        }
    }
    protected function delResponsable($object)
    {
        foreach ($object->getEstResponsableDe() as $reponse ){
            $reponse->removeUniteenseigenementResponsable($object);
        }
    }
    public function setTokenStorage($token_storage)
    {
        $this->token_storage = $token_storage;
    }

    public function setAuthChecker($auth_checker)
    {
        $this->auth_checker = $auth_checker;
    }

}
