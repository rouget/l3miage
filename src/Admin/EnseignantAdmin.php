<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class EnseignantAdmin extends AbstractAdmin
{


    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {

        $datagridMapper
            ->add('id')
            ->add('nom')
            ->add('prenom')
            ->add('volafaire')
            ->add('volaffecte')
            ->add('heurecompl')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('nom')
            ->add('prenom')
            ->add('volafaire')
            ->add('volaffecte')
            ->add('heurecompl')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('nom')
            ->add('prenom')
            ->add('volafaire')
            ->add('volaffecte')
            ->add('heurecompl')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('nom')
            ->add('prenom')
            ->add('volafaire')
            ->add('volaffecte')
            ->add('heurecompl')
            ->add('uniteenseigenement',null,[ 'label' => 'Enseigne'])
            ->add('uniteenseigenement_responsable',null,[ 'label' => 'Est Responsable de'])
            ;
    }
    public function setTokenStorage($token_storage)
    {
        $this->token_storage = $token_storage;
    }

    public function setAuthChecker($auth_checker)
    {
        $this->auth_checker = $auth_checker;
    }
}
