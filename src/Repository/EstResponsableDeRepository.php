<?php

namespace App\Repository;

use App\Entity\EstResponsableDe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstResponsableDe|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstResponsableDe|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstResponsableDe[]    findAll()
 * @method EstResponsableDe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstResponsableDeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstResponsableDe::class);
    }

    // /**
    //  * @return EstResponsableDe[] Returns an array of EstResponsableDe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EstResponsableDe
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
