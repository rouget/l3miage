<?php

namespace App\Repository;

use App\Entity\Decharge;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Decharge|null find($id, $lockMode = null, $lockVersion = null)
 * @method Decharge|null findOneBy(array $criteria, array $orderBy = null)
 * @method Decharge[]    findAll()
 * @method Decharge[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DechargeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Decharge::class);
    }

    // /**
    //  * @return Decharge[] Returns an array of Decharge objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Decharge
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
